import React from 'react'
import styles from './Footer.module.sass'

function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.footer__wrapper}>
        Loan facilities are provided by Pay Later Financial Services Limited who
        are authorised and regulated by the Financial Conduct Authority under
        firm registration number 672306. Registered office: 4th Floor, 33 Cannon
        Street, London, EC4M 5SB, United Kingdom. Registered in England & Wales.
        Registered No: 09020100.
      </div>
    </footer>
  )
}

export default Footer

import React from 'react'
import styles from './PaymentCard.module.sass'
import commonStyles from '../../styles/common.module.sass'

function PaymentCard() {
  const name = 'Contilda Jane-Doe'
  const cardNumber = '**** **** **** 3021'
  const expires = '07/21'

  const Input = ({ title, value }) => (
    <div className={styles.input}>
      <div className={styles.input__title}>{title}</div>
      <div className={styles.input__value}>{value}</div>
    </div>
  )

  return (
    <section className={styles.paymentCard}>
      <div className={styles.paymentCard__wrapper}>
        <div className={styles.header}>
          <span className={styles.title}>Payment Card Details</span>
          <button className={commonStyles.button}>Change</button>
        </div>
        <div>
          <div className={styles.inputRow}>
            <Input title='Card number' value={cardNumber} />
            <Input title='Expires' value={expires} />
          </div>
          <div className={styles.inputRow}>
            <Input title='Name on card' value={name} />
          </div>
        </div>
      </div>
    </section>
  )
}

export default PaymentCard

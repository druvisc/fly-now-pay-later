import React from 'react'
import Header from '../Header/Header'
import Account from '../Account/Account'
import Balance from '../Balance/Balance'
import PaymentCard from '../PaymentCard/PaymentCard'
import Discount from '../Discount/Discount'
import Summary from '../Summary/Summary'
import Footer from '../Footer/Footer'
import styles from './App.module.sass'

function App() {
  return (
    <>
      <Header />
      <Account />
      <content className={styles.content}>
        <div className={styles.content__wrapper}>
          <section className={styles.sideBar}>
            <Balance />
            <PaymentCard />
            <Discount />
          </section>
          <main className={styles.main}>
            <Summary />
          </main>
        </div>
      </content>
      <Footer />
    </>
  )
}

export default App

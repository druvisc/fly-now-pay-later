import React from 'react'
import commonStyles from '../../styles/common.module.sass'
import money from './images/money.svg'
import styles from './Summary.module.sass'

function Summary() {
  const PlanSummary = () => (
    <>
      <div className={styles.summaryTitle}>Your Plan Summary</div>
      <div className={styles.makePayment}>
        <img src={money} alt='' className={styles.makePayment__image}></img>
        <div className={styles.makePayment__left}>
          <div className={styles.makePayment__text}>
            <div className={styles.makePayment__title}>
              6 monthly instalments of{' '}
              <span className={styles.makePayment__amount}>£133.33 p/m</span>
            </div>
            <div className={styles.makePayment__subtitle}>
              Your repayments are currently done on the{' '}
              <span className={styles.makePayment__date}>12th</span> of each
              month.
            </div>
          </div>
        </div>

        <button className={styles.makePayment__button}>Make a payment</button>
      </div>
    </>
  )

  const TransactionList = ({ transactions }) => (
    <>
      <div className={styles.transactionTitle}>Recent Transactions</div>
      <div className={styles.transactionList}>
        {transactions.map(transaction => (
          <Transaction transaction={transaction} />
        ))}
      </div>
    </>
  )

  const Transaction = ({ transaction }) => {
    const creditType = transaction.creditType.toLowerCase()
    const typeModifier = `transaction__type_${creditType}`
    const valueModifier = `transaction__value_${creditType}`

    return (
      <div className={styles.transaction}>
        <div className={styles.transaction__left}>
          <div className={`${commonStyles.tag} ${styles.transaction__tag}`}>
            {transaction.tag}
          </div>
          <div className={styles.transaction__title}>{transaction.title}</div>
          <div className={styles.transaction__subtitle}>
            {transaction.subtitle}
          </div>
        </div>

        <div className={styles.transaction__right}>
          <div
            className={`${styles.transaction__type} ${styles[typeModifier]}`}
          >
            {transaction.creditType}
          </div>
          <div className={styles.transaction__end}>
            <div className={styles.transaction__date}>{transaction.date}</div>
            <div
              className={`${styles.transaction__value} ${styles[valueModifier]}`}
            >
              <span className={styles.transaction__amount}>
                {transaction.amount}
              </span>{' '}
              <span className={styles.transaction__currency}>
                {transaction.currency}
              </span>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const MoreTransactions = () => (
    <button className={`${commonStyles.button} ${styles.moreTransactions}`}>
      Show more transactions
    </button>
  )

  return (
    <div className={styles.summary}>
      <PlanSummary />
      <TransactionList transactions={Transactions} />
      <MoreTransactions />
    </div>
  )
}

export default Summary

const Transactions = [
  {
    tag: 'Dispresal',
    title: 'Moresand Group Authorised Payment',
    subtitle: '',
    creditType: 'Debit',
    date: '11/01/2019 12:54pm',
    amount: '£1,1781.25',
    currency: 'GBP'
  },
  {
    tag: 'Deposit',
    title: 'Repayment',
    subtitle: 'Deposit taken at time of signup with Moresand Group',
    creditType: 'Credit',
    date: '11/01/2019 12:54pm',
    amount: '£93.75',
    currency: 'GBP'
  },
  {
    tag: 'Account created',
    title: 'You created your account at Moresand Group',
    subtitle: 'The account you chose was a flex interest instalment loan',
    creditType: 'Credit',
    date: '11/01/2019',
    amount: '£1,875.00',
    currency: 'GBP'
  },
  {
    tag: 'Dispresal',
    title: 'Moresand Group Authorised Payment',
    subtitle: '',
    creditType: 'Debit',
    date: '11/01/2019 12:54pm',
    amount: '£1,1781.25',
    currency: 'GBP'
  },
  {
    tag: 'Deposit',
    title: 'Repayment',
    subtitle: 'Deposit taken at time of signup with Moresand Group',
    creditType: 'Credit',
    date: '11/01/2019 12:54pm',
    amount: '£93.75',
    currency: 'GBP'
  }
]

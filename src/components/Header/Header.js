import React from 'react'

import summary from './icons/summary.svg'
import statements from './icons/statements.svg'
import payment from './icons/payment.svg'
import settings from './icons/settings.svg'
import support from './icons/support.svg'
import logout from './icons/logout.svg'

import styles from './Header.module.sass'
import logo from './logo.svg'

function Header() {
  const navigation = [
    { title: 'Summary', icon: summary },
    { title: 'Statements', icon: statements },
    { title: 'Payment', icon: payment },
    { title: 'Settings', icon: settings },
    { title: 'Support', icon: support },
    { title: 'Logout', icon: logout }
  ]

  const Logo = ({ src }) => (
    <a href='/' className={styles.logo}>
      <img src={src} alt=''></img>
    </a>
  )

  const Menu = () => <menu className={styles.menu}>Menu</menu>

  const Navigation = ({ navigation }) => (
    <ol className={styles.ol}>
      {navigation.map(nav => (
        <NavigationLink
          {...nav}
          key={nav.title}
          isActive={nav.title === 'Summary'}
        />
      ))}
    </ol>
  )

  const NavigationLink = ({ icon, title, isActive }) => {
    const liClass = `${styles.li} ${isActive ? styles.li_active : ''}`

    return (
      <li className={liClass}>
        <img src={icon} alt='' className={styles.li__img} />
        <a href={`/${title}`} className={styles.a}>
          {title}
        </a>
      </li>
    )
  }

  return (
    <header className={styles.header}>
      <nav className={styles.nav}>
        {/* Display the logo with a route as part of the navigation. */}
        <Logo src={logo} />
        {/* Small sized device menu placeholder. */}
        <Menu />
        <Navigation navigation={navigation} />
      </nav>
    </header>
  )
}

export default Header

import React from 'react'
import pin from './icons/pin.svg'
import styles from './Account.module.sass'

function Account() {
  const username = 'Continda'
  const account = 23773637
  const location = 'London / United Kingdom'

  const Greeting = ({ username }) => (
    <div>
      Welcome back <span className={styles.username}>{username}</span>
    </div>
  )

  const Account = ({ account }) => <div>Account {account}</div>

  const Location = ({ location }) => (
    <div>
      <img src={pin} alt='' className={styles.logo}></img>
      {location}
    </div>
  )

  return (
    <section className={styles.account}>
      <div className={styles.account__wrapper}>
        <div className={styles.left}>
          <Greeting username={username} />
          <div className={styles.seperator}></div>
          <Account account={account} />
        </div>

        <Location location={location} />
      </div>
    </section>
  )
}

export default Account

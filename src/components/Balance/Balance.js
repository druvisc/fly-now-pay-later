import React from 'react'
import styles from './Balance.module.sass'

function Balance() {
  const title = 'Account Balance'
  const balance = '£1,1781.25'
  const currency = 'GPB'

  return (
    <section className={styles.balance}>
      <div className={styles.balance__wrapper}>
        <div className={styles.title}>{title}</div>
        <div>
          <span className={styles.amount}>{balance}</span>
          <span className={styles.currency}>{currency}</span>
        </div>
      </div>
    </section>
  )
}

export default Balance

import React from 'react'
import add from '../../icons/add.svg'
import styles from './Discount.module.sass'

function Discount() {
  return (
    <section className={styles.discount}>
      <div className={styles.discount__wrapper}>
        <div className={styles.title}>Have a discount code?</div>
        <form className={styles.form}>
          <input className={styles.input} placeholder='Discount Code'></input>
          <button className={styles.button}>
            <img src={add} alt='' />
          </button>
        </form>
      </div>
    </section>
  )
}

export default Discount
